# Planner Tray Icon Extension

---

[[_TOC_]]

---

|       |       |
| :---: | :---: |
| <img align="left" width="100px" src="https://raw.githubusercontent.com/alainm23/planner/e0cb4a69887c8b1861886d5571b6ed5c5d423e33/data/icons/128/com.github.alainm23.planner.svg"> | This a gnome-shell-extension for the [planner](https://github.com/alainm23/planner) app. It shows a todo icon in the taskbar to open the application. |



---

## What it does

The image below shows how it looks on ubuntu 20.04 with the [yaru-dark](https://www.gnome-look.org/p/1252100/) theme installed. It is the leftmost icon, when pressed, the application opens up.

![](./img/icon.png)

---

## How it works

It is recommended that the application is always running in the background for fast startup. Clicking the icon runs the command:

```sh
flatpak run com.github.alainm23.planner
```

In case you installed through a method other than [flatpak](flathub.org/), change the command in the `extension.js` file, line `11`.

```javascript
Util.spawnCommandLine("flatpak run com.github.alainm23.planner")
```

Put the startup command inside the quotes.

----

## How to use it

### Installing

You need gnome-extensions in your environment. You can check if it is available running:

```sh
which gnome-extensions
```

To install gnome-extensions you can use apt:

```sh
sudo apt install gnome-shell-extensions
```

Or flatpak:

```sh
flatpak install flathub org.gnome.Extensions
```

In your terminal, clone the repository to the specified path:

```sh
git clone https://gitlab.com/formigoni/planner-gnome-tray-extension.git ~/.local/share/gnome-shell/extensions/planner-tray-icon@formigoni.gitlab.com
```

### Enabling

Reboot or logout and login again, and enable the extension in the gnome-extensions app like shown below:

![](./img/extensions.png)

---
| | |
|:--:|:--:|
| This project was inspired by [Gnome Shell Geary Tray Icon](https://github.com/TheBigFatTony/gnome-shell-extension-geary-tray-icon), check it out! Its great!   | <img align="right" width="100px" height="100px" src="https://wiki.gnome.org/Apps/Geary?action=AttachFile&do=get&target=geary-3-32-256-logo.png">    |