const St = imports.gi.St;
const Main = imports.ui.main;
const Util = imports.misc.util;
const ExtensionUtils = imports.misc.extensionUtils;

const Me = ExtensionUtils.getCurrentExtension();

let button;

function _showPlanner() {
    Util.spawnCommandLine("flatpak run com.github.alainm23.planner")
}

function init() {
  log(`Initializing ${Me.metadata.name} version ${Me.metadata.version}`);
  button = new St.Bin({ style_class: 'panel-button',
                        reactive: true,
                        can_focus: true,
                        x_expand: true,
                        y_expand: false,
                        track_hover: true });
  let icon = new St.Icon({ icon_name: 'org.gnome.Todo-symbolic',
                           style_class: 'system-status-icon' });

  button.set_child(icon);
  button.connect('button-press-event', _showPlanner);
}

function enable() {
  log(`Enabling ${Me.metadata.name} version ${Me.metadata.version}`);
  Main.panel._rightBox.insert_child_at_index(button, 0);
}

function disable() {
  log(`Disabling ${Me.metadata.name} version ${Me.metadata.version}`);
  Main.panel._rightBox.remove_child(button);
}
